#ifndef ASCIICAST_H
#define ASCIICAST_H

#include <stdint.h>
#include <stdio.h>

enum asciicast_error
{
    ASC_OK = 0,
    ASC_ERR_OPEN,
    ASC_ERR_HEADER,
    ASC_ERR_VERSION,
    ASC_ERR_WIDTH,
    ASC_ERR_PALETTE_LENGTH,
    ASC_ERR_EVENT_FORMAT,
    ASC_ERR_TIME,
    ASC_ERR_TYPE,
    ASC_ERR_DATA,
};

struct asciicast_env
{
    char *name;
    char *value;
};

struct asciicast_theme
{
    uint32_t fg;
    uint32_t bg;
    uint32_t palette[16];
    uint8_t palette_len;
};

struct asciicast_header
{
    uint8_t version;
    uint16_t width;
    uint16_t height;
    int32_t timestamp;
    int32_t duration;
    int32_t idle_time_limit;
    char *command;
    char *title;
    struct asciicast_env *env;
    struct asciicast_theme theme;
    FILE *file;
};

enum asciicast_event_type
{
    ASC_TYPE_CLEARED,
    ASC_TYPE_EOF,
    ASC_TYPE_IN,
    ASC_TYPE_OUT,
    ASC_TYPE_ERR,
};

struct asciicast_event
{
    uint32_t time;
    enum asciicast_event_type type;
    char *data;
};

struct asciicast_header *asciicast_header_new(char *filename);
void asciicast_header_free(struct asciicast_header *header);

void asciicast_read_event(struct asciicast_event *event, struct asciicast_header *header);
void asciicast_clear_event(struct asciicast_event *event);

enum asciicast_error asciicast_get_error(void);

#endif