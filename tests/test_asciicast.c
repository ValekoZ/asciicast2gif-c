#define  _POSIX_C_SOURCE 200809L
#define  _XOPEN_SOURCE 500L

#include <asciicast.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <ftw.h>
#include <assert.h>

int main() {
    char template[] = "/tmp/asciicast_test.XXXXXX";
    char *dir_name = mkdtemp(template);

    if (nftw(dir_name, remove, FOPEN_MAX, FTW_DEPTH | FTW_MOUNT | FTW_PHYS)) {
        perror("nftw");
        exit(EXIT_FAILURE);
    }
    
    assert(asciicast_get_error() == ASC_OK);

    return 0;
}