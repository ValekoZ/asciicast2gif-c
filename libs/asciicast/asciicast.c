#include <asciicast.h>
#include <cjson/cJSON.h>
#include <string.h>
#include <stdlib.h>

enum asciicast_error error = ASC_OK;

/************************************************
 * Create a new asciicast header
 * 
 * This function creates a new asciicast header
 * by reading the header from the given file.
 * 
 * @param filename The filename of the asciicast
 * @return The new asciicast header
 ***********************************************/
struct asciicast_header *asciicast_header_new(char *filename){
    struct asciicast_header *header = calloc(1, sizeof(struct asciicast_header));
    
    if (strcmp(filename, "-") == 0) {
        header->file = stdin;
    } else {
        header->file = fopen(filename, "r");
    }
    if(header->file == NULL){
        error = ASC_ERR_OPEN;
        asciicast_header_free(header);
        return NULL;
    }

    // Read the header
    char *header_line = NULL;
    size_t header_line_len = 0;
    getline(&header_line, &header_line_len, header->file);
    
    cJSON *header_json = cJSON_Parse(header_line);
    if (header_json == NULL) {
        error = ASC_ERR_HEADER;
        asciicast_header_free(header);
        return NULL;
    }
    
    cJSON *version = cJSON_GetObjectItemCaseSensitive(header_json, "version");
    if (!cJSON_IsNumber(version) || version->valueint != 2) {
        error = ASC_ERR_VERSION;
        cJSON_Delete(header_json);
        asciicast_header_free(header);
        return NULL;
    } else {
        header->version = version->valueint;
        cJSON *width = cJSON_GetObjectItemCaseSensitive(header_json, "width");
        if (!cJSON_IsNumber(width)){
            error = ASC_ERR_WIDTH;
            cJSON_Delete(header_json);
            asciicast_header_free(header);
            return NULL;
        }
        header->width = width->valueint;

        cJSON *height = cJSON_GetObjectItemCaseSensitive(header_json, "height");
        if (!cJSON_IsNumber(height)){
            error = ASC_ERR_WIDTH;
            cJSON_Delete(header_json);
            asciicast_header_free(header);
            return NULL;
        }
        header->height = height->valueint;

        cJSON *timestamp = cJSON_GetObjectItemCaseSensitive(header_json, "timestamp");
        if (cJSON_IsNumber(timestamp)) {
            header->timestamp = timestamp->valueint;
        } else {
            header->timestamp = -1;
        }
        
        cJSON *duration = cJSON_GetObjectItemCaseSensitive(header_json, "duration");
        if (cJSON_IsNumber(duration)) {
            header->duration = duration->valueint;
        } else {
            header->duration = -1;
        }

        cJSON *idle_time_limit = cJSON_GetObjectItemCaseSensitive(header_json, "idle_time_limit");
        if (cJSON_IsNumber(idle_time_limit)) {
            header->idle_time_limit = idle_time_limit->valueint;
        } else {
            header->idle_time_limit = -1;
        }

        cJSON *command = cJSON_GetObjectItemCaseSensitive(header_json, "command");
        if (cJSON_IsString(command)) {
            header->command = strdup(command->valuestring);
        } else {
            header->command = NULL;
        }

        cJSON *title = cJSON_GetObjectItemCaseSensitive(header_json, "title");
        if (cJSON_IsString(title)) {
            header->title = strdup(title->valuestring);
        } else {
            header->title = NULL;
        }

        cJSON *env = cJSON_GetObjectItemCaseSensitive(header_json, "env");
        if (cJSON_IsObject(env)) {
            header->env = calloc(1, sizeof(struct asciicast_env));
            cJSON *env_item = env->child;
            int count = 0;
            while (env_item != NULL) {
                header->env = realloc(header->env, (++count + 1) * sizeof(struct asciicast_env));
                header->env[count - 1].name = strdup(env_item->string);
                header->env[count - 1].value = strdup(env_item->valuestring);
                
                header->env[count].name = NULL;
                header->env[count].value = NULL;
                
                env_item = env_item->next;
            }
        } else {
            header->env = NULL;
        }
        
        cJSON *theme = cJSON_GetObjectItemCaseSensitive(header_json, "theme");
        if (cJSON_IsObject(theme)) {
            cJSON *fg = cJSON_GetObjectItemCaseSensitive(theme, "fg");
            char *fg_hex = NULL;
            if (cJSON_IsString(fg)) {
                fg_hex = fg->valuestring;
                header->theme.fg = strtol(fg_hex, NULL, 16);
            } else {
                header->theme.fg = 0xffffff;
            }
            
            cJSON *bg = cJSON_GetObjectItemCaseSensitive(theme, "bg");
            char *bg_hex = NULL;
            if (cJSON_IsString(bg)) {
                bg_hex = bg->valuestring;
                header->theme.bg = strtol(bg_hex, NULL, 16);
            } else {
                header->theme.bg = 0x000000;
            }

            // "palette": "#151515:#ac4142:#7e8e50:#e5b567:#6c99bb:#9f4e85:#7dd6cf:#d0d0d0:#505050:#ac4142:#7e8e50:#e5b567:#6c99bb:#9f4e85:#7dd6cf:#f5f5f5"
            cJSON *palette = cJSON_GetObjectItemCaseSensitive(theme, "palette");
            if (cJSON_IsString(palette)) {
                char *palette_hex = palette->valuestring;
                char *palette_hex_item = strtok(palette_hex, ":");
                int i = 0;
                while (palette_hex_item != NULL && i < 16) {
                    header->theme.palette[i] = strtol(palette_hex_item, NULL, 16);
                    palette_hex_item = strtok(NULL, ":");
                    i++;
                }
                
                if (i != 16 || i != 8) {
                    error = ASC_ERR_PALETTE_LENGTH;
                    cJSON_Delete(header_json);
                    asciicast_header_free(header);
                    return NULL;
                }

                header->theme.palette_len = i;
            } else {
                header->theme.palette[0] = 0x151515;
                header->theme.palette[1] = 0xac4142;
                header->theme.palette[2] = 0x7e8e50;
                header->theme.palette[3] = 0xe5b567;
                header->theme.palette[4] = 0x6c99bb;
                header->theme.palette[5] = 0x9f4e85;
                header->theme.palette[6] = 0x7dd6cf;
                header->theme.palette[7] = 0xd0d0d0;
                header->theme.palette[8] = 0x505050;
                header->theme.palette[9] = 0xac4142;
                header->theme.palette[10] = 0x7e8e50;
                header->theme.palette[11] = 0xe5b567;
                header->theme.palette[12] = 0x6c99bb;
                header->theme.palette[13] = 0x9f4e85;
                header->theme.palette[14] = 0x7dd6cf;
                header->theme.palette[15] = 0xf5f5f5;
                
                header->theme.palette_len = 16;
            }
        } else {
            header->theme.fg = 0xffffff;
            header->theme.bg = 0x000000;
            header->theme.palette[0] = 0x151515;
            header->theme.palette[1] = 0xac4142;
            header->theme.palette[2] = 0x7e8e50;
            header->theme.palette[3] = 0xe5b567;
            header->theme.palette[4] = 0x6c99bb;
            header->theme.palette[5] = 0x9f4e85;
            header->theme.palette[6] = 0x7dd6cf;
            header->theme.palette[7] = 0xd0d0d0;
            header->theme.palette[8] = 0x505050;
            header->theme.palette[9] = 0xac4142;
            header->theme.palette[10] = 0x7e8e50;
            header->theme.palette[11] = 0xe5b567;
            header->theme.palette[12] = 0x6c99bb;
            header->theme.palette[13] = 0x9f4e85;
            header->theme.palette[14] = 0x7dd6cf;
            header->theme.palette[15] = 0xf5f5f5;
            
            header->theme.palette_len = 16;
        }
    }
    
    cJSON_Delete(header_json);
    free(header_line);

    return header;
}

/************************************************
 * Free an asciicast header
 * 
 * Free the memory allocated by
 * asciicast_header_read(), for the header and
 * the variables in the header.
 *
 * @param header The header to free
 * @return void
 ***********************************************/
void asciicast_header_free(struct asciicast_header *header) {
    if (header == NULL)
        return;

    if (header->command != NULL)
        free(header->command);
    
    if (header->title != NULL)
        free(header->title);
    
    if (header->env != NULL) {
        for (struct asciicast_env *env = header->env; env->name != NULL && env->value != NULL; env++) {
            free(env->name);
            free(env->value);
        }
        
        free(header->env);
    }

    if (header->file)
        fclose(header->file);

    free(header);
    
    return;
}

void getArray(char **str, size_t *len, FILE *f){
    if (str == NULL || len == NULL || f == NULL)
        return;
    
    char buf;
    size_t current_len = 1;
    
    int in_quotes = 0;

    while (fread(&buf, 1, 1, f) == 1) {
        if (++current_len > *len) {
            *len = current_len;
            *str = realloc(*str, current_len);
            if (*str == NULL) {
                *len = 0;
                return;
            }
            (*str)[current_len - 1] = '\0';
        }

        (*str)[current_len - 2] = buf;

        if (buf == ']' && !in_quotes) {
            // Read until the next line
            while (fread(&buf, 1, 1, f) == 1) {
                if (buf == '\n')
                    break;
            }
            break;
        } else if (buf == '"' && (*str)[current_len - 2] != '\\') {
            in_quotes = !in_quotes;
        }
    }
}

/************************************************
 * Read an asciicast event
 * 
 * Read an asciicast event from the file
 * associated with the header.
 * 
 * @param event The address to store the event
 * @param header The header to read from
 * @return void
 ***********************************************/
void asciicast_read_event(struct asciicast_event *event, struct asciicast_header *header) {
    char *event_line = NULL;
    size_t event_line_len = 0;

    getArray(&event_line, &event_line_len, header->file);
    if (event_line == NULL) {
        event->type = ASC_TYPE_EOF;
        return;
    }
    
    cJSON *event_json = cJSON_Parse(event_line);
    if (!cJSON_IsArray(event_json)) {
        event->type = ASC_TYPE_ERR;
        error = ASC_ERR_EVENT_FORMAT;
        event->type = ASC_TYPE_ERR;
        cJSON_Delete(event_json);
        free(event_line);
        return;
    }
    
    if (cJSON_GetArraySize(event_json) != 3) {
        error = ASC_ERR_EVENT_FORMAT;
        event->type = ASC_TYPE_ERR;
        cJSON_Delete(event_json);
        free(event_line);
        return;
    }
    
    cJSON *time = cJSON_GetArrayItem(event_json, 0);
    if (!cJSON_IsNumber(time)) {
        error = ASC_ERR_TIME;
        event->type = ASC_TYPE_ERR;
        cJSON_Delete(event_json);
        free(event_line);
        return;
    }
    event->time = time->valuedouble;

    cJSON *type = cJSON_GetArrayItem(event_json, 1);
    if (!cJSON_IsString(type)) {
        error = ASC_ERR_TYPE;
        event->type = ASC_TYPE_ERR;
        cJSON_Delete(event_json);
        free(event_line);
        return;
    }
    if (type->valuestring[1] != '\0') {
        error = ASC_ERR_TYPE;
        event->type = ASC_TYPE_ERR;
        cJSON_Delete(event_json);
        free(event_line);
        return;
    }
    switch (type->valuestring[0]) {
        case 'o':
            event->type = ASC_TYPE_OUT;
            break;
        case 'i':
            event->type = ASC_TYPE_IN;
            break;
        default:
            error = ASC_ERR_TYPE;
            event->type = ASC_TYPE_ERR;
            cJSON_Delete(event_json);
            free(event_line);
            return;
    }
    
    cJSON *data = cJSON_GetArrayItem(event_json, 2);
    if (!cJSON_IsString(data)) {
        error = ASC_ERR_DATA;
        event->type = ASC_TYPE_ERR;
        cJSON_Delete(event_json);
        free(event_line);
        return;
    }
    event->data = strdup(data->valuestring);
    
    cJSON_Delete(event_json);
    
    free(event_line);
    
    return;
}

/************************************************
 * Clear an asciicast event
 * 
 * Clear the given event, freeing the memory
 * allocated by asciicast_read_event().
 * 
 * @param event The event to clear
 * @return void
 ***********************************************/
void asciicast_clear_event(struct asciicast_event *event) {
    if (event == NULL)
        return;

    if (event->data != NULL)
        free(event->data);

    event->data = NULL;
    event->type = ASC_TYPE_CLEARED;
    event->time = 0;

    return;
}


/************************************************
 * Get the last error that occurred
 * 
 * Get the last error that occurred in the
 * asciicast library.
 * 
 * @return The last error that occurred
 ***********************************************/
enum asciicast_error asciicast_get_error() {
    enum asciicast_error err = error;
    error = ASC_OK;
    return err;
}
