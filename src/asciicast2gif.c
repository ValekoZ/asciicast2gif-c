#include <asciicast.h>
#include <vterm.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

static VTerm *vterm;
static VTermScreen *screen;

int rows, cols;

static inline unsigned int utf8_seqlen(long codepoint)
{
  if(codepoint < 0x0000080) return 1;
  if(codepoint < 0x0000800) return 2;
  if(codepoint < 0x0010000) return 3;
  if(codepoint < 0x0200000) return 4;
  if(codepoint < 0x4000000) return 5;
  return 6;
}

/* Does NOT NUL-terminate the buffer */
static int fill_utf8(long codepoint, char *str)
{
  int nbytes = utf8_seqlen(codepoint);

  // This is easier done backwards
  int b = nbytes;
  while(b > 1) {
    b--;
    str[b] = 0x80 | (codepoint & 0x3f);
    codepoint >>= 6;
  }

  switch(nbytes) {
    case 1: str[0] =        (codepoint & 0x7f); break;
    case 2: str[0] = 0xc0 | (codepoint & 0x1f); break;
    case 3: str[0] = 0xe0 | (codepoint & 0x0f); break;
    case 4: str[0] = 0xf0 | (codepoint & 0x07); break;
    case 5: str[0] = 0xf8 | (codepoint & 0x03); break;
    case 6: str[0] = 0xfc | (codepoint & 0x01); break;
  }

  return nbytes;
}

static int dump_cell_color(const VTermColor *col, int sgri, int sgr[], int fg)
{
    /* Reset the color if the given color is the default color */
    if (fg && VTERM_COLOR_IS_DEFAULT_FG(col)) {
        sgr[sgri++] = 39;
        return sgri;
    }
    if (!fg && VTERM_COLOR_IS_DEFAULT_BG(col)) {
        sgr[sgri++] = 49;
        return sgri;
    }

    /* Decide whether to send an indexed color or an RGB color */
    if (VTERM_COLOR_IS_INDEXED(col)) {
        const uint8_t idx = col->indexed.idx;
        if (idx < 8) {
            sgr[sgri++] = (idx + (fg ? 30 : 40));
        }
        else if (idx < 16) {
            sgr[sgri++] = (idx - 8 + (fg ? 90 : 100));
        }
        else {
            sgr[sgri++] = (fg ? 38 : 48);
            sgr[sgri++] = 5;
            sgr[sgri++] = idx;
        }
    }
    else if (VTERM_COLOR_IS_RGB(col)) {
        sgr[sgri++] = (fg ? 38 : 48);
        sgr[sgri++] = 2;
        sgr[sgri++] = col->rgb.red;
        sgr[sgri++] = col->rgb.green;
        sgr[sgri++] = col->rgb.blue;
    }
    return sgri;
}

static void dump_cell(const VTermScreenCell *cell, const VTermScreenCell *prevcell)
{
  // If all 7 attributes change, that means 7 SGRs max
  // Each colour could consume up to 5 entries
  int sgr[7 + 2*5]; int sgri = 0;

  if(!prevcell->attrs.bold && cell->attrs.bold)
    sgr[sgri++] = 1;
  if(prevcell->attrs.bold && !cell->attrs.bold)
    sgr[sgri++] = 22;

  if(!prevcell->attrs.underline && cell->attrs.underline)
    sgr[sgri++] = 4;
  if(prevcell->attrs.underline && !cell->attrs.underline)
    sgr[sgri++] = 24;

  if(!prevcell->attrs.italic && cell->attrs.italic)
    sgr[sgri++] = 3;
  if(prevcell->attrs.italic && !cell->attrs.italic)
    sgr[sgri++] = 23;

  if(!prevcell->attrs.blink && cell->attrs.blink)
    sgr[sgri++] = 5;
  if(prevcell->attrs.blink && !cell->attrs.blink)
    sgr[sgri++] = 25;

  if(!prevcell->attrs.reverse && cell->attrs.reverse)
    sgr[sgri++] = 7;
  if(prevcell->attrs.reverse && !cell->attrs.reverse)
    sgr[sgri++] = 27;

  if(!prevcell->attrs.strike && cell->attrs.strike)
    sgr[sgri++] = 9;
  if(prevcell->attrs.strike && !cell->attrs.strike)
    sgr[sgri++] = 29;

  if(!prevcell->attrs.font && cell->attrs.font)
    sgr[sgri++] = 10 + cell->attrs.font;
  if(prevcell->attrs.font && !cell->attrs.font)
    sgr[sgri++] = 10;

  if(!vterm_color_is_equal(&prevcell->fg, &cell->fg)) {
    sgri = dump_cell_color(&cell->fg, sgri, sgr, 1);
  }

  if(!vterm_color_is_equal(&prevcell->bg, &cell->bg)) {
    sgri = dump_cell_color(&cell->bg, sgri, sgr, 0);
  }

  if(sgri) {
    printf("\x1b[");
    for(int i = 0; i < sgri; i++)
      printf(!i               ? "%d" :
          CSI_ARG_HAS_MORE(sgr[i]) ? ":%d" :
          ";%d",
          CSI_ARG(sgr[i]));
    printf("m");
  }

  for(int i = 0; i < VTERM_MAX_CHARS_PER_CELL && cell->chars[i]; i++) {
    char bytes[6];
    bytes[fill_utf8(cell->chars[i], bytes)] = 0;
    printf("%s", bytes);
  }
}

static void dump_eol(const VTermScreenCell *prevcell)
{
  if(prevcell->attrs.bold || prevcell->attrs.underline || prevcell->attrs.italic ||
     prevcell->attrs.blink || prevcell->attrs.reverse || prevcell->attrs.strike ||
     prevcell->attrs.font)
    printf("\x1b[m");

  printf("\n");
}

void dump_row(int row)
{
  VTermPos pos = { .row = row, .col = 0 };
  VTermScreenCell prevcell = { 0 };
  vterm_state_get_default_colors(vterm_obtain_state(vterm), &prevcell.fg, &prevcell.bg);

  while(pos.col < cols) {
    VTermScreenCell cell;
    vterm_screen_get_cell(screen, pos, &cell);

    dump_cell(&cell, &prevcell);

    pos.col += cell.width;
    prevcell = cell;
  }

  dump_eol(&prevcell);
}

int main(int argc, char **argv) {
  if (argc != 2) {
    fprintf(stderr, "Usage: %s <asciicast file>\n", argv[0]);
    return 1;
  }

  struct asciicast_header *header = asciicast_header_new(argv[1]);
  
  if (header == NULL) {
    fprintf(stderr, "Error: %d\n", asciicast_get_error());
    return 1;
  }
  
  printf("Version: %d\n", header->version);
  printf("Width: %d\n", header->width);
  printf("Height: %d\n", header->height);
  printf("Timestamp: %d\n", header->timestamp);
  printf("Duration: %d\n", header->duration);
  printf("Idle time limit: %d\n", header->idle_time_limit);
  printf("Command: %s\n", header->command);
  printf("Title: %s\n", header->title);
  printf("Env:\n");
  for (int i = 0; header->env[i].name != NULL; i++) {
    printf("  %s=%s\n", header->env[i].name, header->env[i].value);
  }
  printf("Theme:\n");
  printf("  fg: #%06x\n", header->theme.fg);
  printf("  bg: #%06x\n", header->theme.bg);
  printf("  palette:\n");
  for (int i = 0; i < header->theme.palette_len; i++) {
    printf("    #%06x\n", header->theme.palette[i]);
  }

  rows = header->height;
  cols = header->width;

  vterm = vterm_new(rows, cols);
  vterm_set_utf8(vterm, true);

  screen = vterm_obtain_screen(vterm);

  vterm_screen_reset(screen, 1);

  struct asciicast_event event;
  asciicast_read_event(&event, header);

  while (event.type != ASC_TYPE_EOF) {
    switch (event.type) {
      case ASC_TYPE_OUT:
        vterm_input_write(vterm, event.data, strlen(event.data));
        break;
      case ASC_TYPE_IN:
      case ASC_TYPE_CLEARED:
      case ASC_TYPE_EOF:
        break;
      case ASC_TYPE_ERR:
        fprintf(stderr, "Error: %d\n", asciicast_get_error());
        return 1;
      default:
        fprintf(stderr, "Unknown event type: %d\n", event.type);
        return 1;
    }

    asciicast_read_event(&event, header);
  }

  for(int row = 0; row < rows; row++) {
    dump_row(row);
  }

  vterm_free(vterm);

  asciicast_header_free(header);

  return 0;
}